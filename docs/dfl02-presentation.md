# Présentation de l'environnement

## Le bureau Xfce

L'environnement graphique (le "bureau")  que vous utilisez se nomme "Xfce".  
Voici les éléments à votre disposition au lancement de votre session :

![Votre bureau Xfce personnalisé par Debian-Facile](img/02-presentation/01-desktop.png)

**Les lanceurs de bureau** : des raccourcis vers votre répertoire personnel 
 et la corbeille.

**La barre de tâches ou panel** en bas de l'écran, contient tous les outils de 
votre bureau :

- le bouton du **Menu Whisker** pour afficher votre menu d'applications classées
par catégories,
- plusieurs **lanceurs** pour accéder rapidement à vos fichiers, votre navigateur 
Internet **Firefox**, votre client mail, votre lecteur multimédia ou la suite **LibreOffice**
- la **liste des fenêtres** ouvertes,
- un **menu d'aide rapide** pour ouvrir les cahiers du débutant, le manuel 
utilisateur ou chercher de l'aide dans la documentation en ligne ou sur le forum,
- une **zone de notification**,
- le réglage du **volume sonore**,
- une **horloge** qui lance votre calendrier au clic,
- le **bouton d'actions** pour quitter votre session, éteindre, mettre en veille
ou redémarrer votre ordinateur.

Un clic-droit sur le bureau affiche un menu permettant de gérer le "dossier 
Bureau", mais aussi de configurer votre fond d'écran ou votre menu (affichage 
des applications ou non) :

![Xfce : menu de bureau](img/02-presentation/02-desktopmenu.png)

### Le menu Whisker, votre menu d'applications

Le "menu" en bas à gauche de votre bureau se nomme "Whisker". 
Celui-ci regroupe différents éléments :

- 1 - Une poignée pour le redimensionner à votre convenance. Maintenez le 
clic-gauche dans le coin du menu pour le redimensionner.
- 2 - Le bouton de déconnexion pour fermer votre session, arrêter, redémarrer ou
mettre en veille votre ordinateur.
- 3 - Le bouton pour ouvrir le gestionnaire de paramètres.
- 4 - Les catégories dans lesquelles vos applications sont classées.
- 5 - Les applications présentes dans la catégorie actuelle, ici le contenu de la 
catégorie "Accessoires".
- 6 - Une barre de recherche permettant de trouver une application depuis son nom 
ou sa description.

Le menu Whisker est configurable depuis un clic-droit sur son icône : vous pourrez 
modifier la présentation du menu, l'action de la souris au survol du pointeur et 
l'affichage (ou non) des différents boutons.

![Le menu complet : Whisker](img/02-presentation/03-whisker.png)

### Liste des applications

Xfce propose également sa liste des applications disponibles depuis les 
différents menus (icône en forme de loupe pour "rechercher" un logiciel) ou 
grâce au raccourci clavier [Alt]+[F3] :

![Xfce : liste des applications](img/02-presentation/05-applist.png)

### Le HandyMenu

DFiso intégrait, dans ses versions précédentes, un menu d'applications configurable 
destiné aux débutantes et débutants souhaitant disposer d'une interface simple, 
le **HandyMenu**.  
Si vous désirez retrouver cette application sur une nouvelle version de DFiso, 
téléchargez le paquet debian 
[depuis ce lien](https://salsa.debian.org/debian-facile-team/projetsdf/handymenu/-/blob/main/handymenu_4.5-2_all.deb) 
puis installez-le en mode graphique (clic-droit sur le paquet : ouvrir avec Gdebi) ou 
depuis votre émulateur de terminal (Menu XFCE : Système : Terminal Xfce) puis entrez 
cette commande :

    sudo apt install ./handymenu_4.5-2_all.deb

Vous retrouverez le HandyMenu dans vos applications et pourrez le rajouter à votre 
barre de tâches.

## Thunar : votre gestionnaire de fichiers

Le gestionnaire de fichiers par défaut du bureau Xfce se nomme **Thunar**. Ce 
programme vous permet de naviguer dans vos dossiers "Images", "Documents", etc, 
avec l'aide de la souris (en effectuant un double-clic sur un dossier pour 
parcourir son contenu), ou du clavier (naviguez parmi les dossiers avec les 
flèches du clavier puis pressez la touche [Enter] pour l'ouvrir). Son rôle est 
d'afficher à l'écran le contenu de ces dossiers : vos données personnelles. 
Le gestionnaire de fichiers Thunar est accessible depuis le menu d'applications 
Xfce de votre barre de tâches, sous le nom "gestionnaire de fichiers", ou depuis 
l'icône "répertoire personnel" sur le bureau. 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/f21d59d2-5acc-42a0-9018-7c9921089579)**

### Présentation simplifiée du gestionnaire de fichiers

Lorsque le gestionnaire de fichiers affiche votre dossier personnel, un certain 
nombre d'informations et d'outils sont déjà à votre disposition :

![le gestionnaire de fichiers Thunar](img/02-presentation/06-thunarexplain.png)

- 1 - La barre de titre : elle renseigne sur le programme lancé, le nom du dossier 
consulté. Elle intègre les "boutons d'actions" qui permettent, lors d'un 
clic-gauche de la souris, de minimiser la fenêtre (alors disponible dans la 
barre de tâches), maximiser la fenêtre (elle occupera tout l'écran), ou fermer la 
fenêtre.
- 2 - La barre de menu : elle permet d'agir sur les fichiers ou dossiers 
(copier, coller, effacer, renommer, etc), de changer d'emplacement, de style 
d'affichage (en icônes, liste, liste compacte), ou de fermer la fenêtre, toujours
à l'aide du clic-gauche.
- 3 - La barre de navigation : elle comporte trois boutons permettant d'atteindre 
respectivement le dossier précédemment visité, le prochain dossier visité, et le 
dossier parent (celui dans lequel se trouve le dossier actuellement affiché).
Elle indique aussi le dossier dans lequel vous êtes, le(s) dernier(s) dossier(s) 
visité(s) et permet de vous y rendre en cliquant dessus. 
- 4 - La barre d'onglets : elle indique les dossiers ouverts dans la fenêtre 
active. Les onglets Thunar fonctionnent comme ceux de votre navigateur internet 
et permettent d'ouvrir plusieurs dossiers dans une seule fenêtre.
- 5 - Le panneau latéral : il affiche les dossiers principaux (dossier personnel, 
corbeille, système de fichiers, bureau), vos raccourcis (dossiers préférés) et 
les volumes externes (clés USB, disques durs externes, smartphones reliés en USB, 
cartes mémoire...). Un clic-gauche sur un libellé affichera le dossier sélectionné. 
Un clic-central ouvrira le dossier dans un nouvel onglet (pratique pour déplacer 
ou copier des données).  
Le panneau latéral peut aussi afficher l'arborescence de votre système, 
c'est-à-dire l'intégralité de vos dossiers et fichiers classés hiérarchiquement.
Vous pouvez masquer/afficher le panneau latéral avec le raccourci clavier 
"Ctrl"+"b".
- 6 - Le cadre principal : il affiche le contenu du dossier. Si le dossier 
sélectionné contient d'autres dossiers, ils seront affichés avant les fichiers 
"simples" (photos, documents PDF, etc) et classés, par ordre alphabétique par 
défaut.
- 7 - La barre de statut : elle affiche le nombre d'éléments dans le dossier 
concerné, indique si un ou plusieurs fichiers sont sélectionnés, ainsi que la 
place restante sur le système de fichiers affiché. 

Le gestionnaire de fichiers va vous permettre de consulter vos données, les 
classer et les modifier. 

### Consultation des données avec votre gestionnaire de fichiers

La consultation de vos données est simple. Lancez votre gestionnaire de fichiers 
qui s'ouvrira par défaut sur votre dossier personnel. Vous pouvez alors choisir 
d'ouvrir un dossier spécifique selon la nature des données à consulter.

- Pour visiter et ouvrir un dossier, placez le pointeur de votre souris 
dessus : un double clic-gauche ouvrira le dossier dans la même fenêtre. 
Un clic-central ouvrira le dossier sélectionné dans un onglet de la fenêtre.  
Vous pouvez également cliquer sur un des raccourcis situés dans le panneau 
latéral gauche de votre fenêtre Thunar.
- Pour ouvrir un fichier, placez le pointeur dessus : un double clic-gauche 
ouvrira le fichier avec l'application assignée par défaut. 
Un clic-droit ouvrira un menu contextuel qui vous permettra, entre autres 
choses, d'ouvrir le fichier avec l'application de votre choix.

### Gestion des périphériques externes

Pour accéder au contenu d'un support externe (clé USB, Disque dur externe, 
smartphone, carte mémoire...), vous avez deux possibilités :  
- Via une fenêtre du gestionnaire de fichiers Thunar : Il apparaîtra dans le 
volet gauche de la fenêtre sous le titre "Périphériques". Cliquez dessus pour 
afficher son contenu.  
- Sur votre bureau : Une icône au nom de votre clé USB (ou autre support externe) 
apparaîtra sur le bureau. Double-cliquez dessus pour afficher son contenu.

![Clé USB visible sur le bureau et dans le volet gauche de Thunar.](img/02-presentation/07-usb1.png)

#### Déconnecter un support externe en toute sécurité

Pour déconnecter une clé USB (ou autre support externe) en toute sécurité pour 
son contenu, deux manières sont possibles :  
- En effectuant un clic-droit sur l'icône du bureau, puis "Éjecter le volume".  
- Par un clic-gauche sur le logo "Eject", présent à droite de votre clé USB 
(ou autre...) dans la fenêtre du gestionnaire de fichiers.  

![Éjecter votre support externe en toute sécurité.](img/02-presentation/08-usb2.png)

### Sélection des données

Pour sélectionner plusieurs éléments, placez votre pointeur dans un espace vide 
de votre fenêtre, puis clic-gauche maintenu pour sélectionner les éléments 
désirés puis relâchez votre souris.

![Sélection multiple à la souris](img/02-presentation/09-thunarselection1.png)

Pour retirer un ou plusieurs éléments de la 
sélection, vous pouvez pointer l'élément, puis clic-gauche tout en maintenant 
la touche [Ctrl] de votre clavier.

![Sélection d'éléments en maintenant la touche 'Ctrl', puis en cliquant sur ceux-ci.](img/02-presentation/10-thunarselection2.png)

Le détail dans l’initiation simplifiée des cahiers du débutant (chap.2.2.2.7).

Une fois vos éléments sélectionnés, vous pouvez effectuer les actions de modifications 
détaillées dans le chapitre "Modifications" ci-après.

### Classement des données

Vous allez me dire : "je fais bien ce que je veux avec mes données"... et vous 
avez bien raison !

Cependant, certaines applications iront chercher plus facilement vos données 
dans leurs dossiers spécifique. Ainsi, le logiciel de capture d'écran 
enregistrera dans le dossier "Images", votre navigateur Internet enregistrera 
vos données dans le dossier "Téléchargements", votre lecteur de musique 
cherchera dans votre dossier "Musique", et ainsi de suite...

Thunar vous permet de créer des raccourcis afin de faciliter l'accès à vos 
dossiers les plus utilisés.

Pour créer un raccourci, il suffit de "glisser-déposer" le dossier désiré dans 
le panneau latéral : clic-gauche maintenu sur le dossier sélectionné, puis 
déplacez votre pointeur dans le panneau latéral, entre deux raccourcis existants. 
Le pointeur doit prendre la forme d'une main et d'une petite chaîne pour indiquer 
que vous créez un raccourci. Votre dossier sera alors toujours accessible d'un 
simple clic.

!["glisser-déposer" d'un dossier pour créer un raccourci dans le panneau latéral](img/02-presentation/11-thunarraccourci.png)

### Modifications des données depuis le menu contextuel

Le menu contextuel déclenché par un clic-droit sur un dossier ou un fichier, 
vous permettra d'effectuer une série de modifications sur le dossier ou fichier 
sélectionné.

![Thunar : menu contextuel](img/02-presentation/12-thunarcontext.png)

Dans notre exemple, le menu contextuel vous propose les actions suivantes :

- Ouvrir avec l'application par défaut : ici, la visionneuse d'images Ristretto.
- Ouvrir avec une autre application : si vous choisissez cette option, une 
fenêtre vous permettra de naviguer dans votre système afin de sélectionner une 
application différente (commençant par /usr/bin/nom de l'application).
- Envoyer vers : cette option permet de partager par courriel, placer comme
lanceur ou différentes autres options selon le type de fichier sélectionné.
- Couper : cette action supprimera le fichier/dossier sélectionné dans le but 
d'être collé ailleurs. Il vous suffit ensuite de naviguer jusqu'au dossier de 
destination, d'effectuer un clic-droit dans la fenêtre du dossier de destination
et de choisir "coller".
- Copier : cette action laissera un exemplaire du fichier/dossier sélectionné 
sur place et fera une copie dans le dossier de destination selon la même 
procédure que pour "couper".
- Déplacer dans la corbeille : cette action supprimera le fichier/dossier 
sélectionné pour le déplacer directement dans votre corbeille (voir le chapitre 
suivant)
- Renommer : cette action vous permet de modifier le nom du fichier/dossier 
sélectionné.
- Créer une archive : cette action vous permet de compresser un ou plusieurs 
fichier(s) ou dossier(s) sélectionné(s). Dans la fenêtre qui s'affiche alors, 
choisissez un dossier de destination en naviguant dans le panneau latéral, puis 
indiquez le nom de votre archive avec son type. Cliquez sur "Nouveau" pour lancer 
la création de l'archive.
- Définir comme fond d'écran est explicite et n'apparaît que sur une image
- Propriétés : cette action vous permet de modifier le nom du fichier, 
l'application par défaut pour ouvrir ce fichier, lui attribuer un "emblème", ou 
gérer les permissions du fichier/dossier sélectionné.

### Suppression de vos données

La fameuse "Corbeille". Elle est accessible directement depuis le panneau 
latéral de votre fenêtre Thunar. Une icône "pleine" vous indique que des 
éléments se trouvent dans la corbeille.

![vider la corbeille](img/02-presentation/13-thunartrash.png)

Pour vider la corbeille et supprimer définitivement les éléments qui s'y 
trouvent, faites un clic-droit sur l'icône appropriée, puis choisissez 
"Vider la corbeille". 
Vous pouvez aussi passer par le menu "Fichier" de la barre d'outils puis 
"Vider la corbeille".


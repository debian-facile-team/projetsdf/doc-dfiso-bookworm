**Bienvenue sur Debian !**

Debian (plus précisément Debian GNU/Linux) est un système d'exploitation, 
tout comme l'est windows®, produit de microsoft®.

Votre système, ainsi que ce manuel sont réalisés par des membres bénévoles 
de la communauté [Debian-Facile](https://debian-facile.org/).  
N'hésitez pas à vous rendre sur le forum Debian-Facile.org pour nous donner 
des retours sur votre expérience d'utilisateur ! :)

![Bandeau de la communauté Debian-Facile](img/logos/debianfacile.png)

Cette documentation est là pour vous permettre d'apprendre à utiliser 
immédiatement votre ordinateur sous Debian (organiser vos fichiers personnels, 
connecter votre ordinateur à Internet, naviguer sur le Web, échanger des 
e-mails, utiliser un traitement de texte, installer une imprimante, scanner 
vos documents...)

Si vous avez la curiosité (très bonne qualité !) d'approfondir vos connaissances 
sur Debian, vous trouverez votre bonheur dans 
[les cahiers du débutant](http://debian-facile.org/projets/lescahiersdudebutant).

Vous y apprendrez toutes les possibilités de ce système, ainsi que sa philosophie.

![Les cahiers du débutant](img/logos/les_cahiers_du_debutant.png)

Bonne découverte !

Debian-Facile © WTFPL 2024 
[Projets Debian-Facile](https://debian-facile.org/wiki#les-projets-debianfacile)

---

Note : ce manuel contient des liens vers les tutoriels vidéos proposés par 
Debian-Facile sur sa chaîne Peertube (le YouTube libre et décentralisé).  

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/c/debian.facile_channel/videos)**

Lorsque vous verrez l'icône appropriée, n'hésitez pas à cliquer sur le lien 
"tutoriel vidéo", il vous mènera directement vers le tutoriel dédié.

-![Sommaire](img/logos/sommaire.png)-

**1 - [Premier lancement](#premier-lancement) -**  
L'écran de connexion, le message d'accueil, les premières choses que vous voyez en allumant votre système pour la première fois...  

**2 - [Présentation de l'environnement](#presentation-de-lenvironnement) -**  
Une présentation des différents composants de votre espace de travail numérique : votre gestionnaire de fenêtres, votre menu, votre gestionnaire de fichiers...  

**3 - [Applications Internet](#applications-internet) -**  
Votre navigateur Firefox, votre client de messagerie Thunderbird, le partage de données avec Transmission...  

**4 - [Applications Bureautique](#applications-bureautique) -**  
Votre éditeur de texte Mousepad, la suite bureautique complète LibreOffice, numériser ou imprimer des documents...  

**5 - [Applications Multimédia](#applications-multimedia) -**  
Visionner un film avec VLC, profiter de la musique avec Rhythmbox ou sauvegarder vos CD avec Asunder...  

**6 - [Les petits outils](#les-petits-outils) -**  
La capture d'écran, la recherche de fichiers, les sauvegardes ou le formatage de clés USB...  

**7 - [Configuration de l'environnement](#configurez-votre-environnement) -**  
Configurer la connexion sans fil ou un VPN, l'écran d'authentification ou la barre de tâches, l'apparence ou le comportement de la souris...  

**8 - [Administration du système](#administration-du-systeme) -**  
Mises à jour des applications, installation ou suppression de paquets, gestion des utilisateurs ou des mots de passe...  

**9 - [Aller plus loin avec Debian](#aller-plus-loin-avec-debian) -**  
Anonymat et vie privée, le terminal, supprimer les outils facilitants de Dfiso...  

**10 - [Sources et licence](#sources-licence) -**  
Les sources sont publiées sous licence libre bien sûr !!

  

Une table des matières détaillée est disponible en fin de manuel (lien en bas de page).

Bonne lecture ...

# Configurez votre environnement

Le bureau Xfce dispose de son outil de configuration centralisé : 
depuis le menu Whisker, cliquez sur l'icône du gestionnaire des 
paramètres en haut à droite du menu :

![Icône des paramètres](img/07-config/01-parametres-icon.png)

Celle-ci donne accès au gestionnaire de paramètres vous permettant 
de configurer votre système.

![gestionnaire de paramètres](img/07-config/02-parametres.png)

## Configurez votre connexion Wi-Fi

Si vous souhaitez configurer une nouvelle connexion de type "sans-fil", 
suivez la procédure suivante.  
Ça se passe en bas à droite, dans la zone de notification. Avec un clic-gauche 
de la souris, vous obtenez des informations sur vos connexions en cours. Le 
réseau filaire (Wired connection 1), est le câble qui relie votre ordinateur à
votre boîtier internet, la box internet.  
En-dessous de "Disponible", ma box apparaît. Bien sûr ce sera différent chez 
vous. Quel est le nom de ma box internet ? Normalement, c'est écrit sur la box 
elle-même, ou sur le carton qui la contenait, ou sur la documentation qui vous a
été fournie. 
Je sais donc que le nom de ma box est "arpibox", et je clique dessus :

![Wifi : se connecter à un réseau](img/07-config/03-wifi1.png)

J'obtiens alors une boîte de dialogue qui me demande un mot de passe, qui m'a
été fourni avec ma box. Dans mon cas, ce mot de passe est écrit au dos de ma 
box. C'est une suite de lettres, minuscules et majuscules, et de chiffres.  
Cliquez sur «Afficher le mot de passe» pour voir ce que vous écrivez. Un piège à
éviter : ne pas confondre la lettre O ou I en majuscule et les chiffres 0 ou 1 
qui se ressemblent beaucoup.

![Wifi : renseigner le mot de passe](img/07-config/04-wifi2.png)

Ensuite cliquez sur "Se connecter", la recherche se fait et, au bout de 
quelques secondes, une notification vous prévient que la connexion a abouti.

![Wifi : notification de connexion](img/07-config/05-wifi3.png)

Vous pouvez dorénavant surfer grâce au Wifi.

### Configuration des connexions

L'icône de connexion réseau de la barre de tâches vous permet également de couper
les réseaux filaire ou wifi, configurer et modifier votre configuration, ou 
obtenir des informations sur votre liaison. Pour cela, un simple "clic-droit" sur 
l'icône concernée, les entrées sont explicites :

![Wifi : menu de configuration](img/07-config/06-wifi4.png)

Obtenez les informations sur vos connexions :

![Wifi : informations sur la connexion](img/07-config/07-wifi5.png)

Pour modifier vos propriétés de connexion, sélectionnez "Modifier les connexions" 
dans le menu contextuel puis cliquez sur le menu à 3 bandes :

![Modifier vos connexions au réseau](img/07-config/08-wifi6.png)

### Configuration d'une connexion VPN

Le **VPN**, pour **V**irtual **P**rivate **N**etwork, est un protocole qui
permet de faire passer vos échanges sur internet via un "tunnel" privé et 
sécurisé. DFiso intègre une préconfiguration utilisant le VPN public et gratuit
de l'association FDN ([French Data Network](https://www.fdn.fr)).  
Pour en profiter, rien de plus simple qu'un clic-gauche sur l'icône de connexion
internet ; "Connexions VPN" ; puis cochez la case "**FDN Publique**".

![VPN : utiliser le VPN public de la FDN](img/07-config/09-vpn1.jpg)

Une fois le VPN actif, un petit cadenas s'incruste sur l'icône de connexion
et un message vous informe :

![VPN activé](img/07-config/10-vpn2.jpg)

## Configurez votre écran de connexion

**Configurer votre écran de connexion : Menu > Paramètres > 
LightDM GTK+ Paramètres d'Apparence** (Votre mot de passe vous sera demandé)

![configuration de Lightdm](img/07-config/11-lightdm-cfg.png)

Vous pouvez ainsi :

- changer le fond affiché à la connexion ;
- définir la police utilisée ;
- spécifier le thème GTK utilisé (l'apparence de la fenêtre de connexion) ;
- préciser le thème d'icônes (les petits boutons en haut à droite) ;
- activer ou désactiver le verrouillage numérique ou le lecteur d'écran 
  (dans l'onglet "Divers").

Une fois votre configuration définie, cliquez sur "Enregistrer" afin de 
sauvegarder vos choix.

## Configurer la barre de tâches Xfce

Dans la configuration par défaut, le tableau de bord comprend : le bouton 
WhiskerMenu, les lanceurs, la liste des fenêtres, le menu d'aide, la zone de 
notifications, l'horloge et le bouton d'action vous permettant de quitter la 
session ou éteindre votre ordinateur.

Pour configurer votre panel, un clic-droit dessus puis choisir 
"Préférences du tableau de bord"

![Xfce : configuration du tableau de bord](img/07-config/12-panel-cfg1.png)

Les fenêtres qui s'affichent alors, vous permettent de définir l'apparence et le 
contenu du tableau de bord :

![configuration du panel Xfce : affichage](img/07-config/13-panel-cfg2.png)

**Affichage :**  
- Orientation : pour un panel horizontal ou vertical.  
- Verrouillage : affiche/masque les poignées pour déplacer votre panel.  
- Masquage automatique : votre panel ne s'affiche que sur demande en approchant 
le pointeur.  
- Taille : hauteur en pixels du panel.  
- Largeur : en pourcentage de la taille de l'écran.  
- Largeur automatique : permet d'adapter le panel au nombre d'éléments inclus.

![configuration du panel Xfce : apparence](img/07-config/14-panel-cfg3.png)

**Apparence :**  
- Arrière-Plan : définir le style (celui du thème, une image ou une couleur) 
ainsi que l'opacité en pourcentage pour le fond du panel.  
- Opacité : définir le niveau de transparence par défaut du panel tout entier.

![configuration du panel Xfce : éléments](img/07-config/15-panel-cfg4.png)

**Éléments :**  
- Modifier l'emplacement des éléments avec les flèches "haut"/"bas" de la liste de 
droite.  
- Ajouter un élément avec le bouton "+" de la liste de droite.  
- Supprimer un élément avec le moins "-" rouge.  
- Configurer un élément avec le bouton édit "i".

## Configurer votre bureau

**Configurer votre bureau :**  
- **Menu > Paramètres > Bureau**  
- **Ou clic droit sur votre bureau > Paramètres du bureau**  

Ces paramètres vous permettront d’accéder à la gestion et à la configuration de vos 
fonds d’écran et des icônes système sur le bureau. 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/e816b27a-9a51-4251-990c-62db7f2bee03)**

### Gérer ses fonds d'écran

![Xfce : configuration du fond d'écran](img/07-config/16-desktop-wall.png)

L'onglet ouvert par défaut "Fond d'écran" vous permet de choisir votre fond 
d'écran parmi ceux déjà compris dans la distribution mais aussi de rajouter ceux
que vous souhaitez.  
Il suffit de cliquer sur une des images disponibles dans le panneau de présentation 
pour que celle-ci soit automatiquement appliquée à votre bureau et ensuite, cliquer 
sur "Fermer" une fois la modification effectuée pour que celle-ci soit persistante.

**Ajouter des fonds d'écran :**

Pour rajouter des images afin que celles-ci soient disponibles dans le menu de 
sélection il suffit de placer vos images dans un dossier spécifique (de votre 
choix, par exemple "~/Images/fonds d'écran") puis d'effectuer un clic-droit sur 
une des images dans votre gestionnaire de fichier et choisir "Définir comme fond 
d'écran".  
Notez que si vous déplacez votre image, elle ne sera plus accessible comme fond 
d'écran : veillez à choisir un emplacement dédié.

**Où trouver des fonds d'écran ?**  
Voici quelques sites externes pour faire votre choix.

- [Gnome-Look.org](http://gnome-look.org)
- [VladStudio](http://www.vladstudio.com/wallpapers/)
- [thePaperWall.com](http://thepaperwall.com/index.php)
- [WallpapersWide.com](http://wallpaperswide.com/)
- [Unsplash.com](https://unsplash.com/)
- [ZoneWallpaper.free.fr](http://zone.wallpaper.free.fr/)

### Menus du bureau

![Xfce : menu des applications](img/07-config/17-desktopmenu-apps.png)

Cet onglet vous permet de paramétrer :
- Le menu du bureau, qui apparaît quand vous faites un clic droit sur l'arrière plan du bureau.
- Le menu de la liste des fenêtres, qui apparaît quand vous faites un clic-central sur l'arrière plan du bureau.

### Icônes du bureau

L'onglet "Icônes" vous permet de définir le type d'icônes affichées sur votre 
bureau :

![Xfce : les icônes de bureau](img/07-config/18-desktopmenu-icons.png)

- Aucune : désactiver l'affichage des icônes du bureau. Notez que si vous 
choisissez cette option, le menu contextuel du bureau ne sera plus affiché.
- Icônes des applications réduites : les icônes minimisées sont affichées dans 
la barre de tâches et sur le bureau.
- Icônes de fichiers / lanceurs : (par défaut) affiche tous les 
éléments présents dans le dossier "Bureau".

Il est possible de sélectionner dans la liste présentée en bas ("Icônes par 
défaut"), des icônes qui apparaîtront sur votre bureau et pourront permettre si 
vous le souhaitez d'ajouter des raccourcis vers des applications ou des dossiers.

## Peaufiner l'interface utilisateur

**Personnaliser l'apparence : Menu > Paramètres > Apparence**

Cette section vous permet de configurer l'apparence interne des fenêtres, le 
thème d'icône utilisé, la police de caractères, et la barre de menu des fenêtres

![configuration de l'apparence générale et des décorations](img/07-config/19-apparence.png)

**Style des fenêtres**

Dans cet onglet vous choisissez parmi des thèmes déjà installés l'apparence 
intérieure de vos fenêtres (jeu de couleurs), il vous suffit de cliquer sur les 
choix présents afin de constater directement les changements, ce qui aide à faire
son choix rapidement. Toutes les fenêtres du système auront l'aspect que vous 
choisirez ici.

**Icônes du système**

Ici sont disponibles quelques packs d'icônes. Il suffit de sélectionner celui 
choisi afin de constater les changements, dans la barre de tâches notamment, et 
appliquer les icônes à tout le système (si vous voulez avoir un aperçu plus 
vaste, ouvrez le gestionnaire de fichiers afin de voir plus d'icônes changées).

**Polices du système**

C'est dans cet onglet que les paramètres de la police d'écriture se règlent :  
- Police par défaut : permet de choisir la police qui sera utilisée pour 
l'affichage global sur le système ainsi que sa taille.  
- Rendu : permet de peaufiner le rendu visuel afin d'avoir un aspect plus 
esthétique.  
- DPI : permet de modifier la résolution de l'écran si les polices d'écriture 
n'apparaissent pas à la bonne taille.

**Paramètres des menus, boutons et sons**

Configuration semi-détaillée :  
- Style de la barre d'outils : permet de choisir si dans celle-ci, les icônes et
le texte doivent apparaître individuellement ou ensemble.  
- Menus et boutons : permet de choisir l'affichage des images/icônes dans les 
menus et boutons.  
- Sons et événements : permet de choisir quels sons sont joués par le système 
afin de rendre celui-ci plus ou moins avertissant.

### Configuration rapide grâce au DFiso-Theme

Afin de configurer très rapidement votre environnement, Dfiso vous propose
le **DFiso-Theme** qui vous permet de passer en un clic d'un thème clair à un
thème sombre, et d'une interface minuscule à une interface large.  
Pour en profiter, direction la section "Accessoires" de votre menu ; **DFiso-Theme**

![DFiso-Theme : section Accessoires du menu](img/07-config/20-dfisotheme1.png)

![DFiso-Theme : fenêtre principale](img/07-config/21-dfisotheme2.png)

Cliquez simplement sur le bouton de votre choix, les modifications sont immédiates.  
Pour revenir à votre thème de départ, cliquez sur "Annuler".

## Configurer vos fenêtres

**Personnaliser les fenêtres : Menu > Paramètres > Gestionnaire des fenêtres**

Configurer vos décorations de fenêtres, raccourcis clavier et l'attribution du 
focus (fenêtre active).

![configuration du thème des fenêtres](img/07-config/22-fenetrescfg.png)

### Choisir la décoration des fenêtres

La décoration de fenêtre est l'encadrement comprenant les boutons des fenêtres 
de vos programmes.
Dans l'onglet "Style" (ouvert par défaut), il suffit de choisir dans la 
liste disponible un thème qui vous convient : un simple clic-gauche sur celui-ci
affiche les changements.

### Raccourcis clavier

Vous pouvez modifier dans cet onglet les raccourcis permettant de fermer une 
fenêtre, de circuler parmi les fenêtres ouvertes, etc. La méthode est simple : 
double-clic (deux pressions du clic-gauche) sur la ligne du raccourci à 
modifier, puis tapez simplement votre nouveau raccourci dans la fenêtre de 
dialogue qui s'affiche.

Note : pour les raccourcis clavier attachés à des applications, c'est dans la 
"Configuration Détaillée", section "Clavier" qu'il faut chercher.

### Focalisation

L'attribution du focus (fenêtre active) se règle dans cet onglet :  
- Cliquer pour focaliser : vous devez effectuer un clic-gauche sur une partie de
la fenêtre pour la rendre active.  
- La focalisation suit la souris : nul besoin de cliquer, la fenêtre active est 
celle où se trouve votre pointeur. Le délai de focalisation se règle grâce au 
curseur de cette section.  
- Focalisation des nouvelles fenêtres : détermine si les fenêtres nouvellement 
créées obtiennent automatiquement ou non le focus.  
- Mettre au premier plan lors d'un clic : permet de cliquer pour rendre la 
fenêtre active, même si une autre fenêtre réclame le focus.

### Avancé... quasi aventuriers

Cet onglet de la configuration du gestionnaire de fenêtres vous permet des 
réglages plus fins. Les libellés sont très explicites et les changements 
immédiats.

## Configurer votre souris

La configuration de la souris passe par plusieurs sections : la focalisation, le
thème et le comportement du pointeur.

### Thème et taille du pointeur

**Configurer votre thème de curseur : Menu > Paramètres > Souris et pavé 
tactile > Onglet "Thème"**

Dans cet onglet, vous pourrez choisir le thème et la taille de votre curseur.

### Simple ou Double-Clic

**Configurer le simple ou double-clic : Menu > Paramètres > Paramètres de gestionnaire de fichiers > Onglet "Comportement"**

Dans l'onglet "Comportement", choisissez simple-clic ou double-clic.

![configuration du comportement du curseur](img/07-config/23-mouse-clic.png)

**Clic-automatique :** en mode "simple-clic", vous pouvez effectuer un 
clic-gauche automatique en laissant le pointeur sur une icône de fichier/dossier
ou une icône d'application. Le délai avant le clic automatique se configure depuis 
le curseur de la fenêtre de préférences.

### Droitier ou gaucher

**Configurer le simple ou double-clic : Menu > Paramètres > Souris et 
pavé tactile > Onglet "Périphériques"**

La fenêtre qui s'ouvre vous permet de faire votre choix entre droitier/gaucher. 
(Inverser le clic gauche et le clic droit).

### Tap-to-Clic

**Activer le "Tap-o-Clic" : Menu > Paramètres > Souris et pavé tactile > 
Onglet "Périphériques"**

Le "Tap-to-Clic" est la possibilité de déclencher un "clic" de la souris en 
tapant avec un ou plusieurs doigts sur le touchpad (le pavé tactile) de votre 
ordinateur portable.  
Pour activer cette fonction, direction "Configuration détaillée" > "Souris".  
L'onglet "Pavé tactile" et ses réglages sont explicites :

![cliquer avec le pavé tactile](img/07-config/24-touchpadtap.png)

## Configurer l'heure et la date

**Configurer l'heure et la date : Menu > Paramètres > Date et heure**

Cette option est disponible si vous désirez changer manuellement l'heure, la 
date ainsi que votre fuseau horaire.

L'heure est par défaut réglée de manière automatique. Vous pouvez passer en 
mode "manuel" et modifier les réglages de votre machine :

![date et heure : déverrouiller pour configurer](img/07-config/25-date-cfg.png)

## Configurer la disposition du clavier

**Configurer la disposition du clavier : Menu > Paramètres > Clavier > 
Onglet "Disposition"**

La fenêtre Disposition de clavier apparaît et vous pouvez faire vos réglages :

![configuration de la disposition du clavier](img/07-config/26-kbd-cfg.png)

- Utiliser les paramètres par défaut : cocher cette option applique les réglages 
qui ont été définis lors de l'installation de votre système. Décochez-la pour 
afficher les réglages suivants.
- Modèle de clavier : pour régler l'agencement de votre clavier (généralement 
par défaut c'est 105 touches)
- Changer l'option de disposition : Si vous avez installé plusieurs dispositions, 
cette option permet de définir un raccourci clavier permettant de passer d'une 
disposition à l'autre.
- Position de la touche compose : la touche "compose" n'existe pas sur tous 
les claviers, et son action est simulée par des combinaisons de touches 
(altGr + shift par exemple). 
- Dispositions de clavier : ajoutez ici les différents claviers à gérer


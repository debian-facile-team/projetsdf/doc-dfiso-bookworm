# Applications Internet

## Naviguez avec Firefox

**Ouvrir Firefox : Menu > Internet > Firefox ESR**

[Firefox](https://www.mozilla.org/fr/firefox/desktop/) est un navigateur Web 
libre et gratuit, développé et distribué par la Fondation Mozilla.

![Firefox](img/03-internet/01-firefox.png)

La fenêtre principale se compose des éléments suivants (de haut en bas) :

- **La barre d'onglets** affiche  les différentes pages ouvertes.
- **La barre d'outils** affiche le bouton de retour, la barre d'adresse, le 
champ de recherche, les marque-pages, les outils et le menu Firefox.
- **La fenêtre de navigation** qui affiche le contenu des pages web.

La **personnalisation** et la **configuration** de Firefox passent par son 
menu principal qui s'affiche lors d'un clic sur l'icône à 3 bandes.

- La première ligne du menu **"Synchroniser et enregistrer les données"** vous permet, 
si vous avez préalablement créé un compte Firefox, de synchroniser vos données (comme vos 
marque-pages, historique, mots de passe, onglets ouverts et modules complémentaires 
installés) sur tous vos appareils.
- **"Nouvel onglet"** permet d'ouvrir un onglet dans la fenêtre active de firefox.
- **"Nouvelle fenêtre"** permet d’ouvrir une 2ème fenêtre avec le navigateur Firefox.
- **"Nouvelle fenêtre privée"** va ouvrir une 2ème fenêtre mais 
cette fois-ci, les mots de passe, les cookies et l’historique seront automatiquement 
effacés, ne laissant aucune trace à la fin de la session.
- **"Marque-pages"** vous permet de consulter et organiser vos marque-pages.
- **"Historique"** vous permet de consulter et organiser votre historique de navigation.
- **"Téléchargements"** ouvre une nouvelle fenêtre présentant vos derniers téléchargements.
- **"Mots de passe"** permet d’accéder directement aux paramètres 
relatifs aux mots de passe enregistrés dans Firefox (about${:}$preferences#privacy).
- **"Extensions et thèmes"** conduit aux extensions installées (activées et désactivées) et 
vous permet de gérer l'apparence de firefox.
- **"Imprimer"** ouvre la fenêtre d’aperçu avant impression
- **"Enregistrer sous"** va vous aider à copier sur votre disque dur, dans un répertoire 
spécifié, la page web active dans Firefox. A noter que l’extension ScrapBee facilite 
la capture et la gestion de pages web sur le disque dur 
[https://addons.mozilla.org/fr/firefox/addon/scrapbee/?src=search](https://addons.mozilla.org/fr/firefox/addon/scrapbee/?src=search).
- **"Rechercher dans la page"** active la barre de recherche. Ne reste plus qu’à 
saisir le mot clé recherché.
- Vient ensuite la ligne de **zoom** : c’est explicite.
- **"Paramètres"** permet d’accéder à la rubrique générale des paramètres 
de Firefox. 
- **"Outils supplémentaires"** permet de lancer le "mode édition" de Firefox : 
vous pourrez déplacer les outils, les ajouter depuis le menu dans la barre d’outils et inversement... 
bref, vous composez l’apparence de votre navigateur simplement avec un glisser-déposer des 
éléments sur l’interface. Cette section permet aussi l'affichage du code source de la page active.
- **"Aide"** envoie à l’aide en ligne de Firefox, vous pourrez découvrir toutes les possibilités 
de votre navigateur : 
[https://support.mozilla.org/fr/products/firefox](https://support.mozilla.org/fr/products/firefox).

### Firefox et les moteurs de recherche

**Le moteur de recherche par défaut : DuckDuckGo** 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/6c8618b1-6b51-4e78-85ff-6e27ab050bf7)**  
DuckDuckGo est un moteur de recherche libre et respectueux de votre vie privée.

**Les autres moteurs de recherche :**  
- **Qwant :** Le moteur de recherche Européen qui se veut respectueux de la vie 
privée de ses utilisateurs.  
- **Wikipedia :** Vous permet de faire une recherche directement dans l'encyclopédie Libre.  
- **Google :** Faut-il le présenter ? Le moteur de recherche le plus utilisé, connu 
pour la pertinence de ses résultats, mais aussi pour ses nombreuses controverses...

#### Utiliser momentanément un autre moteur de recherche

Pour utiliser un autre moteur le temps d'une recherche, saisissez votre 
recherche dans la barre de Firefox. Les moteurs de recherche installés apparaissent 
en dessous. Cliquez sur celui que vous souhaitez utiliser pour cette recherche.

![Moteurs de recherche dans Firefox : exemple d'une recherche avec Google](img/03-internet/02-firefox-search1.png)

#### Installer un nouveau moteur de recherche dans Firefox

Si vous souhaitez installer un nouveau moteur de recherche dans Firefox (en plus 
de ceux déjà présents), rendez-vous sur la page web de celui-ci (sur lilo.org 
par exemple).  
Cliquez sur le lien "Ajouter à Firefox" qui vous demandera si vous autorisez 
l'installation du module de recherche complémentaire. Cliquez sur "Continuer 
l'installation" : le module s'installe puis Firefox vous demande (encore) 
l'autorisation pour ajouter ce moteur de recherche.

![Ajouter un nouveau moteur de recherche](img/03-internet/03-firefox-search2.png)

![Autoriser un nouveau moteur de recherche](img/03-internet/04-firefox-search3.png)

![Moteur de recherche activé](img/03-internet/05-firefox-search4.png)

#### Remplacer le moteur de recherche principal

Si vous voulez un autre moteur de recherche que DuckDuckGo par défaut, direction le 
menu principal de Firefox (l'icône à 3 bandes) puis sélectionnez "Préférences".  
Dans la fenêtre qui s'affiche, sélectionnez "Recherche" puis cliquez sur le menu 
déroulant qui liste vos moteurs de recherche : sélectionnez le moteur par défaut que 
vous souhaitez.

![Firefox : sélection du moteur par défaut](img/03-internet/06-firefox-search5.png)

### Firefox : ajouter des extensions

Pour ajouter des fonctionnalités à votre navigateur Firefox, direction le 
menu principal > Extensions et thèmes. Dans l'onglet qui s'ouvre, 
sélectionnez "Recommandations" et faites votre choix parmi les modules proposés 
([https://addons.mozilla.org/fr/firefox/extensions/](https://addons.mozilla.org/fr/firefox/extensions/)).  
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/mVtYxxv2DJ3ALSfdfXn99U)**

![Firefox : les extensions](img/03-internet/07-firefox-addons.png)

### Ublock Origin : Bloquer les publicités sur les sites

L'extension Ublock Origin est pré-installée dans votre Firefox. Celle-ci bloque les 
publicités sur les sites Web afin d'obtenir une navigation plus confortable, 
plus rapide, et plus sécurisée.

Ublock Origin est accessible via l'icône en forme de bouclier rouge en haut à 
droite de la fenêtre de Firefox :

![Firefox : Ublock Origin](img/03-internet/08-firefox-ublock1.png)

Le gros logo bleu qui s'affiche en cliquant sur l'icône uBlock Origin permet de 
désactiver le blocage des publicités. Cela peut être utile dans certains cas :  
- Pour ne pas priver un site de ses revenus publicitaires.  
- Pour faire vos recherches avec un moteur de recherche qui finance des projets 
via ses revenus publicitaires (Ecosia, Lilo...)  
- Pour visiter certains sites qui vous imposent de le désactiver...  

En visitant certains sites, il peut vous arriver d'obtenir le message suivant :

![Firefox : site bloqué par Ublock Origin](img/03-internet/09-firefox-ublock2.png)

Ce message vous avertit que uBlock Origin bloque l'accès à ce site car le site 
est répertorié dans l'un de ses filtres (ici, dans le filtre "EasyPrivacy").  
En se rendant sur le site du filtre, on apprend à quoi il sert : 
[https://easylist.to/](https://easylist.to/)  
*"EasyPrivacy est une liste de filtres supplémentaires facultative qui supprime 
complètement toutes les formes de suivi (tracking) d'Internet, y compris les 
bogues Web, les scripts de suivi (tracking) et les collecteurs d'informations, 
protégeant ainsi vos données personnelles."*

Le site est donc bloqué pour protéger votre vie privée. Ce site collecte 
potentiellement ce que vous y faites (ce que vous écrivez, ce que vous 
cliquez...) pour connaître vos goûts et préférences afin d'afficher de la 
publicité ciblée. Dans ce cas, vous avez deux possibilités :  
- Désactiver le blocage du site, temporairement ou définitivement, et accéder au 
site malgré l'espionnage potentiel.  
- Quitter le site.

**Note :** Si un site vous paraît bloqué par Ublock Origin de manière injustifiée, 
n'hésitez pas à le faire savoir sur le [forum Debian-Facile](https://debian-facile.org), 
ou mieux, directement à [l'équipe d'Ublock Origin](https://github.com/gorhill/uBlock/) 
(Page des sources du projet, en anglais)

## Le client mail Thunderbird

**Ouvrir Thunderbird : Menu > Internet > Messagerie Thunderbird**

[Thunderbird](https://www.mozilla.org/fr/thunderbird/) est avant tout un 
courrielleur, mais il est aussi un centre de communication qui permet de 
consulter et d'écrire dans les groupes de discussions, de "chater" (discuter en 
ligne), de consulter les flux RSS fournis par certains sites.

Si vous disposez de deux comptes de messagerie ou plus, Thunderbird regroupera 
tout votre courrier de toutes vos boites de courrier électronique en une seule 
interface pratique. 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/5hG5ZGq6JwhnUavYShxA73)**

### Configurer Thunderbird

Au premier démarrage, Thunderbird fournit une interface provisoire de 
configuration ainsi qu'une fenêtre au premier plan qui permet de créer un compte
chez un fournisseur partenaire de la fondation Mozilla.  
Si vous souhaitez profiter de cette option, remplissez les champs et 
laissez-vous guider, la configuration sera automatique.

Si vous n'êtes pas intéressé vous pouvez cliquer sur "Passer cette étape et 
utiliser mon adresse existante".

Une autre fenêtre s'ouvre. Fournissez alors les renseignements demandés à propos
de votre compte. Vous pouvez choisir si Thunderbird doit retenir le mot de passe
. Si vous décochez cette option vous devrez taper votre mot de passe à chaque 
connexion.

![Thunderbird : configuration du compte](img/03-internet/10-thunderbird-1.png)

Cliquez sur "Continuer". Thunderbird cherche alors dans sa base de donnée les 
paramètres spécifiques à votre compte. Lorsqu'il a fini, cliquez sur "Terminé".

![Thunderbird : vérification des données](img/03-internet/11-thunderbird-2.png)

Thunderbird affiche maintenant les détails de votre compte dans la colonne de 
droite et télécharge tous vos courriels et, suivant votre fournisseur, vos 
contacts (ceci peut prendre un long moment). Cliquez maintenant sur votre compte
pour le dérouler, puis "courrier entrant". L'interface prend alors son apparence
normale :

![Thunderbird : interface par défaut](img/03-internet/12-thunderbird-3.png)

### L'interface de Thunderbird

L'interface de Thunderbird est relativement intuitive. Dans la barre d'outil 
haute vous pouvez relever votre courrier, écrire un nouveau message, ouvrir une 
session de chat, accéder à votre carnet d'adresse, mettre une étiquette à un 
message, ou encore filtrer vos messages. Si vous cliquez sur un message dans le 
bandeau du haut, des options pour le message seront aussi disponibles : répondre
, transférer, archiver, indésirable, etc .

Le panneau de droite est réservé au calendrier «Lightning», très intuitf : un 
clic sur une date ouvrira l'assistant de prise de rendez-vous.

Si vous recherchez un message précis, vous disposez de plusieurs possibilités:

- taper quelques caractères dans la barre de recherche en haut,
- cliquer sur l'intitulé des colonnes ("Sujet", "Expéditeur", "Date", "Taille"):
vos messages seront alors instantanément triés selon le critère choisi, ce qui
vous évitera d'avoir à faire défiler une liste interminable pour retrouver un 
message ancien. En cliquant une deuxième fois sur le même titre de colonne vous 
inversez l'ordre du tri.  
Vous pouvez ainsi personnaliser votre affichage des messages d'une catégorie 
selon vos propres critères avec ces outils.

### Récupérer un ou d'autres compte(s) de messagerie

Si vous disposez d'un ou d'autres comptes de messagerie que vous désirez 
consulter à l'aide de Thunderbird, cliquez maintenant sur les trois petites 
barres horizontales à droite (les paramètres) au bout de la barre de recherche, 
posez votre pointeur sur "Nouveau message", puis dans le menu déroulant cliquez 
sur "Compte courrier existant" et reprenez la procédure de récupération d'un 
compte de messagerie.

### Les paramètres

Les trois petites barres horizontales regroupent tous les paramètres. C'est la 
présentation moderne. Si vous désirez changer ce comportement pour retrouver des
menus classiques, cliquez sur les paramètres, posez le pointeur de votre souris 
sur "préférences", et cochez "Barre des menus".

Il est toutefois conseillé de visiter les préférences où vous pourrez définir 
vos niveaux de filtres anti-spam, paramétrer vos étiquettes et avoir des options 
comme n'utiliser qu'un seul mot de passe pour tous vos comptes, entre autres.

## La visioconférence avec FramaTalk

**Accéder à Framatalk : ouvrez Firefox et rendez-vous à l'adresse 
[https://framatalk.org/accueil/](https://framatalk.org/accueil/)**

Ne pas utiliser Skype... c'est déjà la base pour éviter de confier ses 
conversations à Microsoft et ses annonceurs, ou à la NSA …

Encore faut-il trouver une alternative pour communiquer avec ses proches ou 
collègues en visio-conférence. C'est là que le portail Framasoft offre une 
solution libre et sécurisée : [Framatalk](https://framatalk.org/accueil/)

Framatalk fait partie des nombreux services proposés par l'association Framasoft. 
Contrairement à certains services qui seront fermés à partir de 2020 
([voir liste ici](https://framablog.org/2019/09/24/deframasoftisons-internet/)), 
Framatalk sera pérénisé.

![visioconférence avec Framatalk](img/03-internet/13-framatalk.png)

Debian-Facile recommande l'utilisation de FramaTalk comme alternative fiable
, libre et sécurisée à skype.

## Partage P2P avec Transmission

**Ouvrir Transmission : Menu > Internet > Transmission**

Transmission est un client 
[BitTorrent](http://fr.wikipedia.org/wiki/BitTorrent_%28protocole%29), ce qui 
signifie qu'il va gérer des fichiers de type ".torrent". Ces fichiers sont 
utilisés sur les serveurs de partage pour identifier des données.

Pour simplifier, le fichier ".torrent" contient un inventaire détaillé des 
données à partager, les adresses des serveurs où on peut trouver ces données et 
un tableau de ces données "découpées" en petits morceaux qui seront ensuite 
partagées entre les différents utilisateurs du protocole BitTorrent.

Afin de profiter de ce protocole, vous devez passer par un client BitTorrent. 
Debian intègre Transmission, un client torrent léger, facilement configurable et
qui offre une interface claire.

### Lancement

Transmission se lance depuis le menu whisker mais 
est aussi appelé lors du téléchargement d'un fichier de type 
".torrent" ou d'un "lien-magnet" depuis votre navigateur internet. 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/hmuABLu7jgfTmJRMhJubkr)**

### Récupération de données

Transmission est utilisé pour récupérer des données en utilisant le p2p au lieu 
de liens direct html.

Par exemple, pour récupérer l'image ISO de Debian, vous pouvez utiliser les 
liens html qui vous donnent directement accès aux 
[images ISO d'installation](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/) ou les 
[fichiers ".torrent"](https://cdimage.debian.org/debian-cd/current/amd64/bt-cd/) 
qui vont aller chercher tous les "petits morceaux" de l'ISO chez celles et ceux 
qui la partagent.

![Récupération d'un fichier torrent](img/03-internet/14-torrent-1.png)

Votre fichier ".torrent" est par défaut ouvert par Transmission depuis Firefox.

![Ouverture d'un fichier torrent avec Transmission](img/03-internet/15-torrent-2.png)

La fenêtre d'ajout de Transmission s'affiche alors, vous invitant à ouvrir le 
fichier. Si vous utilisez Transmission pour la première fois, un petit 
avertissement s'affiche puis la fenêtre d'options s'ouvre :

![Transmission : options du torrent](img/03-internet/16-torrent-3.png)

Une fois ajouté, le torrent apparaît dans la liste des données partagées. Une 
fois le téléchargement terminé, vos données seront disponibles dans votre 
dossier ~/Téléchargements.

![Transmission en partage](img/03-internet/17-torrent-4.png)

Par principe, on demande aux utilisateurs de ce type de protocole de laisser les
données en partage afin de "faire tourner" le plus longtemps possible…

### Configuration

Transmission se configure très simplement depuis le menu "Édition > Préférences". 
Les entrées sont explicites. Vous n'aurez pas besoin de changer quoi que ce soit 
pour utiliser Transmission au départ.

et voilà ... partagez bien.


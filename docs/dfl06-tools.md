# Les petits outils

## Capture d'écran

**Ouvrir la Capture d'écran : Menu > Accessoires > Capture d'écran** 
(Ou touche "Impr Écran de votre clavier)

La fonction de capture d'écran est assurée par le programme "xfce4-screenshooter".  
Le logiciel de capture d'écran est très simple d'utilisation et vous offre 
différentes options :  
- capture de l'écran entier  
- capture de la fenêtre active (celle qui est au premier plan)  
- capture dans une zone à sélectionner avec la souris

![Outil de capture d'écran](img/06-tools/01-screenshooter.png)

Vous pouvez utiliser le retardateur pour avoir le temps d'effectuer une action 
avant la capture d'écran. Une fois prise, la capture s'enregistre dans votre 
dossier "Images". Une confirmation vous sera demandée, vous permettant de 
sauvegarder votre image ailleurs, ou de changer son nom.

## Recherche de fichiers

**Recherche de fichiers : Menu > Accessoires > Recherche de fichiers Catfish**

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/215bfb37-8272-400d-98a7-aa8a31549d30)**  
Pour retrouver vos données, Debian intègre Catfish, un outil de recherche simple
et pratique. Vous pouvez le lancer depuis votre menu d'applications ou directement 
depuis votre gestionnaire de fichiers Thunar : menu Fichiers > "Rechercher dans ce dossier" 
ou depuis le menu contextuel.

Une fois lancé, il vous suffit de renseigner le nom recherché dans la zone de
texte en haut de la fenêtre, puis spécifier le dossier de recherche dans le menu
déroulant en haut à gauche. Un clic sur l'icône de recherche pour obtenir les
résultats.

![Catfish : votre outil de recherche de données](img/06-tools/02-catfish.png)

Vous pouvez afficher le panneau latéral des filtres en cliquant sur le menu à 3
bandes.  
Catfish analyse le contenu de votre système une fois par session de travail : si
vous recherchez un fichier récent, pensez à "Rafraîchir l'index de recherche" depuis
le menu principal.

**La recherche de fichiers directement depuis Thunar**

![Thunar intègre un outil de recherche de données](img/06-tools/02-recherche.png)

Vous pouvez également utiliser Thunar; votre gestionnaire de fichiers intègre 
un outil de recherche : il vous suffit de cliquer sur l'icône appropriée 
(la loupe en haut à droite de la fenêtre Thunar), de sélectionner votre dossier 
de départ dans le panneau latéral (ici c'est le Système de fichiers) puis de 
taper votre recherche dans la zone de saisie "Search:..." :

Un clic-droit sur un résultat vous permettra d'ouvrir le dossier parent, ouvrir 
le fichier directement et effectuer différentes actions.

## Gestionnaire d'archives

La compression des données permet non seulement de gagner de la place, mais 
aussi de protéger la structure d'un dossier et de ses sous-dossiers dans 
l'optique d'une sauvegarde.  
Cette fonction est confiée à **Engrampa**.

**Pour ouvrir une archive**, effectuez un double-clic sur le fichier concerné.

### Création d'une archive

**Pour créer une archive**, depuis votre navigateur de fichiers Thunar :

- sélectionnez le ou les dossiers concernés dans votre gestionnaire de fichiers,
puis clic-droit sur la sélection > Créer une archive.

![archives : sélection des données à archiver](img/06-tools/03-archive1.png)

- sélectionner le dossier de destination de l'archive, son nom et son extension 
puis créez l'archive.

![archives : sélection du dossier de destination](img/06-tools/04-archive2.png)

![archives : création de l'archive, compression](img/06-tools/05-archive3.png)

- si vous ouvrez l'archive avec Engrampa, vous retrouvez l'arborescence de vos 
données :

![archives : aperçu du contenu](img/06-tools/06-archive4.png)

## Sauvegardes avec Déjà Dup

**Logiciel de sauvegardes : Menu > Accessoires > Sauvegardes**

Quel que soit votre niveau en informatique ou l'état de votre matériel, personne
n'est à l'abri d'une mauvaise manipulation, d'un incident technique, d'un orage 
violent, d'une tasse de thé sur le clavier, d'un chat qui se soulage dans 
l'unité centrale ou tout simplement de l'usure normale des disques durs et de leurs
éléments mécaniques.

Les capacités des disques durs augmentent et nous sommes tous tentés de stocker 
de plus en plus de données (photos de famille, vidéos, copies privées de films, 
etc.). Le risque de perdre une grande quantité de données augmente en même temps,
c'est pourquoi nous vous conseillons de procéder à des sauvegardes régulières de
vos données personnelles ainsi que de vos mots de passe ou données de courriel.

**Déjà Dup** 
([https://wiki.gnome.org/Apps/DejaDup](https://wiki.gnome.org/Apps/DejaDup)) 
est un outil de **sauvegarde simplifié**.  
Il permet de sauvegarder vos données dans un 
dossier local, un disque externe, un réseau local, distant ou de type "cloud".

Il permet aussi le **chiffrement complet** et la protection par **mot de passe** de votre sauvegarde.

**Déjà Dup** dispose d'une **interface très claire** ne nécessitant aucune 
connaissance informatique.

Pour les détails du fonctionnement de Déjà Dup, reportez-vous au chapitre 9.2.1 des 
Cahiers du débutant (intégrés à DFiso par défaut).  
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/4d88abc1-da17-429d-a34b-0e73d04d4215)**

## Espace disque disponible

**Analyseur d'utilisation des disques : Menu > Accessoires > 
Analyseur d'utilisation des disques**

La gestion des données demande parfois de "faire de la place" et nettoyer un peu
son dossier personnel. Pour faire le ménage, il faut en premier lieu identifier 
les données qui prennent le plus de place. Votre système intègre un logiciel d'analyse d'utilisation des disques (qui se nomme Baobab) :

![lanceur de l'outil d'espace disque](img/06-tools/07-applist-baobab.png)

Sélectionnez le point de montage à analyser :

![Baobab : sélection du point de montage à scanner](img/06-tools/08-baobab1.png)

Affichez un rendu visuel de l'espace disque utilisé :

![Baobab : visualisation de l'espace disque](img/06-tools/09-baobab2.png)

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/2a2d4d53-523f-4219-8639-8934165e8a86)**

## Informations système

**Informations système : Menu > Système > Informations et Benchmarks du système**

Votre système intègre *hardinfo*, un outil listant toutes les spécificités 
de votre système et de votre matériel :

![outils de collecte d'informations système](img/06-tools/10-hardinfo.png)

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/3b92e63f-ad7e-4e96-b524-591781e9fe4c)**

## Gestionnaire Bluetooth

**Gestionnaire Bluetooth : Menu > Paramètres > Gestionnaire Bluetooth**

La technologie bluetooth permet de relier sans fil des appareils pour échanger 
des données (imprimante, hauts-parleurs, clavier, etc). Si votre ordinateur 
est équipé de cette technologie, vous pourrez gérer les appareils connectés à 
l'aide de l'application blueman.

- lancement : vous trouverez blueman dans la section "Paramètres" de la "Liste 
des Applications". Une fenêtre va s'ouvrir et une nouvelle icône apparaîtra dans 
la zone de notification. 

![lanceur de l'outil de gestion du bluetooth](img/06-tools/11-blueman1.png)

- lancement automatique : pour activer le lancement automatique, allez dans le 
menu whisker, section "Paramètres", puis "Session et démarrage". Dans l'onglet "Démarrage 
automatique des applications", vous pouvez cocher "Applet Blueman". 

![blueman : gestion du bluetooth au démarrage](img/06-tools/12-blueman2.png)

## Formater un disque

**Utilitaire de disques : Menu > Accessoires > Disques**

L'utilitaire de disque intégré vous permet de formater simplement et facilement 
une clé USB, un disque dur externe ou une carte SD pour votre appareil photo 
numérique. Vous pouvez aussi obtenir une liste d'informations à propos de vos 
disques durs internes.  
Pour formater facilement une clé USB ou une carte SD…

- Insérez votre support puis sélectionnez-le dans le panneau latéral. Vérifiez 
de ne pas sélectionner un disque interne :

![Formater une clé USB : sélection du disque](img/06-tools/13-disks1.png)

- Cliquez sur "Formater la partition" depuis le menu disponible en cliquant sur 
l'icône représentant des petits engrenages :

![Formater une clé USB : menu](img/06-tools/14-disks2.png)

Une fenêtre s'ouvre pour configurer le formatage. Ici, je choisis "Compatible" 
afin d'utiliser ma clé USB sur différents systèmes d'exploitation.  
Vous pouvez aussi sélctionner "Effacer" afin d'écraser les données existantes et 
ainsi empêcher toute récupération de données (conseillé pour la sécurité).

![Formater une clé USB : options du formatage](img/06-tools/15-disks3.png)

Cliquez ensuite sur "Suivant" puis "Formater" :

![Formater une clé USB : confirmation](img/06-tools/16-disks4.png)

Vous pouvez maintenant démonter et débrancher votre clé USB pour l'utiliser 
partout.

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/7c440747-c8ea-440e-a1c8-3afff729eea2)**


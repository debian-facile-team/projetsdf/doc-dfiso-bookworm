# Applications Multimédia

## Lecteur vidéo VLC

**Ouvrir VLC : Menu > Multimédia > Lecteur multimédia VLC**

Debian-Facile a fait le choix d'utiliser le lecteur multimédia **VLC** pour votre 
système, léger et simple d'utilisation, afin de vous permettre de profiter de 
vos vidéos facilement.

VLC se lance directement avec un double-clic sur un fichier multimédia 
(une vidéo par exemple).
Pour ouvrir un média depuis l'application, direction le menu "Média" > 
"Ouvrir un fichier". Il propose une interface épurée par défaut.

![Le lecteur multimédia VLC](img/05-media/01-vlc.png)

VLC permet aussi de lire des flux réseau (podcasts), écouter une radio en ligne,
capturer l'écran et aussi encoder un flux pour le sauvegarder.

## Le gestionnaire de musique Rhythmbox

**Ouvrir Rhythmbox : Menu > Multimédia > Rhythmbox**

**Rhythmbox** 
([https://wiki.gnome.org/Apps/Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox)) 
est le lecteur de musique par défaut de l'environnement Gnome, mais il 
s'intègre très bien sur les autres bureaux Debian, dont Xfce.  
Il est capable de gérer une large collection de musique

Voici ses fonctionnalités principales :  
- Tri par artistes, albums ou genres.  
- Listes de lecture.  
- Affichage des pochettes et des paroles.  
- Gestion des podcats et des web-radios.  
- Intégration de [Jamendo](http://www.jamendo.com/fr/), 
[Magnatune](http://www.magnatune.com/) et [Last.fm](http://www.lastfm.fr/).  
- Support des lecteurs portables (MTP et iPod).  
- Support des télécommandes infra-rouges.  
- Partage et lecture de musique sur un réseau local.  

### Premier lancement de Rhythmbox

Au premier lancement, Rhythmbox scanne votre dossier "Musique", mais 
vous pouvez ajouter d'autres dossiers à votre bibliothèque musicale.

![Rhythmbox : interface par défaut](img/05-media/02-rhythmbox-1.png)

### Configuration de Rhythmbox

Rhythmbox intègre une interface de réglage des préférences ainsi qu'un système 
de greffons (ou plugins) qui permet d'ajouter des fonctionnalités au lecteur. 
Pour accéder aux différents outils, direction le menu principal (l'icône à 3 bandes).

![Rhythmbox : menu des préférences](img/05-media/03-rhythmbox-2.png)

![Rhythmbox : gestionnaire de préférences](img/05-media/04-rhythmbox-3.png)

Rhythmbox bénéficie d'une aide complète intégrée.  
Laissez votre souris se balader, vous ne risquez rien : une confirmation 
vous sera demandée pour chaque action impliquant la modification ou la 
suppression de vos fichiers musicaux.

## Extraire un CD audio avec Asunder

**Ouvrir Asunder : Menu > Multimédia > Asunder**

Asunder est un logiciel qui vous permettra d'extraire la musique de vos CD audio 
afin de stocker leurs pistes audio dans votre ordinateur ou d'autres appareils 
(baladeur MP3, smartphone etc.) sous forme de fichiers Ogg, Mp3, Wav, Flac, 
WavePack, MusePack, Monkey’s Audio ou AAC. 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/rb3p5yMKYEYz4nwDaYnWyP)**

![Asunder : interface par défaut](img/05-media/05-rip1.png)

L'interface de Asunder comporte quatre boutons :

- Recherche CDDB, qui permet de rechercher les informations du CD audio 
que vous insérez,
- Préférences, qui vous ouvre la fenêtre des réglages (général, fichiers, 
encodeur et avancé),
- A propos, qui ouvre une fenêtre de renseignement et un lien vers le site 
Asunder,
- Extraire, qui lance l'extraction de votre CD audio.

### Utilisation

Insérez un CD audio et ouvrez le logiciel Asunder. Ici, nous allons encoder 
"PWR Up" par AC/DC. Le CD est chargé, scanné et si il est connu de la base de 
données CDDB, les informations s'insèrent automatiquement.

![Asunder : chargement d'un CD audio](img/05-media/06-rip2.png)

A ce stade il vous suffit de cliquer sur le bouton "Extraire" et l'encodage débute 
avec les réglages définis dans les préférences. Par défaut, le CD sera extrait :

- dans votre répertoire personnel avec un fichier liste de lecture M3U, et le
CD ne sera pas éjecté à la fin,
- les fichiers seront extraits dans un dossier de votre dossier personnel avec 
le nom d'artiste et le titre de l'album. Les fichiers s'appelleront 
"Num piste - Artiste - Titre",
- au format OGG.

Vous pouvez suivre la progression puis profiter de votre musique avec 
[Rhythmbox](#le-gestionnaire-de-musique-rhythmbox).

![Asunder : encodage d'un CD audio](img/05-media/07-rip3.png)

### Réglages

Nous allons visiter les onglets de préférences d'Asunder.

#### Onglet Général

![préférences d'Asunder : général](img/05-media/08-rip4.png)

L'onglet Général propose :

- de choisir l'emplacement de destination des conversions, par défaut c'est 
votre répertoire personnel (Home).
- de créer un fichier "liste de lecture M3U", qui vous permet de lancer la 
lecture d'un album en un seul clic (ce fichier tient compte du lieu de stockage 
défini au départ. Si vous déplacez les fichiers, il se peut que ce fichier ne 
fonctionne pas. Vos fichiers audio sont toujours là mais ils ne sont plus liés 
au fichier M3U),
- l'emplacement de votre lecteur CD-ROM,
- la commande d'éjection ou non du CD Audio en fin d'encodage.

#### Onglet Nom de fichier

![préférences d'Asunder : nom de fichier](img/05-media/09-rip5.png)

C'est ici que vous définissez le nom des fichiers encodés. Avec l'aide, vous 
pouvez nommer vos fichiers comme bon vous semble.

#### Onglet Encodeur

![préférences d'Asunder : encodeur](img/05-media/10-rip6.png)

Définissez ici le format de conversion de vos CD audio. Vous pouvez sélectionner
autant de formats que ceux présentés. Si vous sélectionnez plus d'un format de 
conversion, votre dossier de conversion comportera plusieurs fois les mêmes 
titres avec des formats différents.

- Le format le plus connu est le MP3. Ce format est lisible quasiment partout 
(ordinateur, téléphone portable, tablette, lecteur MP3, autoradio…). MP3, est la spécification 
sonore du standard MPEG-1/MPEG-2, du Moving Picture Experts Group (MPEG). 
C'est un algorithme de compression audio (voir aussi codec) capable de réduire 
drastiquement la quantité de données nécessaire pour restituer de l'audio, mais 
qui, pour l'auditeur, ressemble à une reproduction du son original non 
compressé : avec une bonne compression la différence de qualité devenant 
difficilement perceptible,
- Ogg Vorbis est un algorithme de compression et de décompression (codec) audio-numérique, sans brevet, ouvert et libre, plus performant en termes de qualité et de taux de compression que le format MP3, mais moins populaire que ce dernier.

Les autres formats sont pour divers usages :

- Wav, une contraction de WAVEform audio file format, est un standard pour 
stocker l'audio numérique de Microsoft et d'IBM,
- FLAC est un codec libre de compression audio sans perte. À l’inverse des codecs
tels que MP3 ou Vorbis, il n’enlève aucune information du flux audio. Cette 
qualité maximale a pour conséquence une quantité d'informations plus élevée, qui 
tout en étant assez variable se trouve en moyenne être de l'ordre de 50 % de la 
taille du même fichier au format PCM,
- [WavPack](http://www.wavpack.com/) est un format de fichier ainsi qu'un outil 
de compression audio permettant d'encoder (et de restaurer) un flux PCM sans 
perte ou avec perte, et même de cumuler les deux aspects (format hybride, voir 
ci-dessous). Le format est ouvert, et l'outil est libre, distribué selon les 
termes de la licence BSD.
- MusePack est conçu pour produire un encodage dit «transparent» (c'est-à-dire 
indifférenciable du fichier original) à des débits compris entre 160 et 180 
kilobits/seconde,
- Monkey's Audio est un codec de compression audio sans perte. À l'inverse de 
codecs tels que MP3 ou Vorbis, il n'enlève aucune information du flux audio. 
L'extension de Monkey's Audio est: .ape (ape signifiant en anglais "grand singe"
, tandis que monkey signifie "singe"). Les partisans du logiciel libre lui 
préfèrent FLAC,
- AAC est un algorithme de compression audio avec perte de données. Il a pour 
but d’offrir un meilleur ratio qualité/débit-binaire que le format plus ancien 
MPEG-1/2 Audio Layer 3 (plus connu sous le nom de MP3). Pour cette raison, il a 
été choisi par différentes firmes comme Apple ou RealNetworks. C'est aussi le 
standard audio pour YouTube, iPhone, iPod, iPad, Nintendo DSi, Nintendo 3DS, 
iTunes, DivX Plus Web Player et PlayStation 3.

#### Onglet Avancé

![préférences d'Asunder : avancé](img/05-media/11-rip7.png)

Cet onglet permet de régler la méthode de récupération des informations du CD audio et l'utilisation ou pas d'un proxy. Le réglage par défaut pour CDDB est correct. Si vous utilisez un proxy, pensez à mettre les informations pour permettre l'accès à la CDDB.

## Gravez un CD/DVD avec Xfburn

**Ouvrir Xfburn : Menu > Multimédia > Xfburn**

- Pour sauvegarder simplement vos données, vous pouvez les copier, faire un 
"backup" sur un disque dur externe, ou alors graver vos données sur un CD/DVD.
- Pour profiter de votre musique sur votre chaine hi-fi, vous aurez besoin de 
fabriquer un CD au format audio compatible.
- Pour partager Debian et créer un DVD bootable, vous aurez besoin de graver une
image au format ISO. Pour plus de précisions sur la fameuse image ISO, je vous 
conseille la lecture de la 
[documentation léa-linux dédiée](http://lea-linux.org/documentations/Image_ISO).

Pour toutes ces tâches, Xfburn est là !  
Généralement, il vous suffit d'insérer un disque vierge dans le lecteur de votre
ordinateur pour que Xfburn se lance et vous propose de graver le disque.

### Présentation de l'interface

![Xfburn : gravure de CD/DVD](img/05-media/12-burn.png)

La partie haute vous permet de naviguer dans votre répertoire pour ajouter des 
fichiers à graver. Au premier lancement, la partie inférieure vous permet de 
sélectionner le type d'action désiré :

- Graver une image : pour un fichier de type ISO
- Nettoyer un disque : pour effacer les données d'un disque ré-inscriptible 
(Type CD-RW, DVD-RW ou BD-RW)
- Composition de données : sauvegarder des données lisibles par un autre 
ordinateur
- Composition audio : graver un CD de type audio lisible par les chaines hi-fi 
et autoradios

### Graver une image disque

Pour partager Debian ou toute autre distribution GNU/Linux, vous pouvez 
récupérer l'image au format ISO afin de la graver. Dans ce cas, choisir "Graver 
une image". Une fenêtre de sélection surgit alors vous invitant à renseigner 
votre image à graver, le lecteur utilisé (généralement détecté) et d'autres 
options que vous pouvez laisser par défaut.

Une fois votre image sélectionnée, et votre CD/DVD dans le lecteur, cliquez sur 
"Graver l'image" pour lancer la gravure. Votre disque sera éjecté 
automatiquement une fois la gravure achevée.

### Nettoyer un disque

Insérer votre disque ré-inscriptible dans le lecteur, ouvrez Xfburn puis 
choisissez "Effacer un disque". Une fenêtre surgit alors pour confirmer le 
lecteur utilisé et le mode d'effacement (rapide ou total alias sécurisé). 
Faites un clic sur "Effacer" pour lancer l'effacement. Le disque sera éjecté à la 
fin de la procédure.

### Sauvegarder des données

Le mode "Composition de données" vous permet de sauvegarder des données 
sélectionnées grâce à Xfburn. Dans la fenêtre principale, "clic" sur "Composition 
de données". Sélectionner les dossiers à sauvegarder puis "clic" sur "Ajouter". 
Votre sélection sera visible dans la partie inférieure, avec la barre de 
remplissage du support. Vous pouvez continuer à naviguer dans vos données afin 
d'ajouter d'autres dossiers/fichiers.

Une fois votre gravure prête, "clic" sur "Lancer la gravure" en bas à droite. Une 
fenêtre de confirmation s'ouvre alors pour indiquer le lecteur et d'autres 
options explicites. Une fois configurée, "clic" sur "Graver la composition" pour 
lancer la gravure.

Une fois la gravure achevée, le disque sera éjecté et vous pourrez fermer la 
fenêtre de gravure.

### Créer un disque audio

Pour graver un disque compatible avec votre chaîne hi-fi, choisissez 
"CD audio" dans la fenêtre principale. De la même façon que pour la 
composition de données (chapitre précédent), utilisez le navigateur dans la
partie supérieure pour sélectionner vos fichiers audio qui seront ajoutés à la 
liste de gravure dans la partie inférieure grâce au bouton "Ajouter".

Comme pour la composition de données, confirmation et lancement de la gravure, 
puis éjection du disque. Vous pouvez maintenant profiter de vos fichiers audio 
sur votre installation de salon traditionnelle.

## Contrôleur de volume PavuControl

**Ouvrir PavuControl : Menu > Multimédia > Contrôle du volume PulseAudio**

Le réglage du son passe par PavuControl. Pour l'ouvrir, direction le l’icône 
de volume de votre panel.  
Votre carte son devrait être reconnue automatiquement et la fenêtre du Mixer vous 
proposera les différents réglages possibles.  
Pour modifier votre périphérique de sortie, cliquez sur "Configuration" :

![PavuControl : régler le volume audio](img/05-media/13-pavucontrol.png)

## La visionneuse d'images Ristretto

**Ouvrir Ristretto : Menu > Graphisme > Visionneur d'images Ristretto**

Ristretto est un visionneur d'images ultra-minimal disponible en effectuant 
un double-clic sur une image dans le gestionnaire de fichiers Thunar.

![Ristretto : le visionneur d'images](img/05-media/14-ristretto.png)

C'est la visionneuse d'images par défaut sur DFiso et permet une consultation 
rapide et facile de vos fichiers images.

Une interface claire et simple avec juste assez de fonctions pour naviguer dans 
vos images facilement. Si vous désirez éditer une image, un clic-droit dans 
Ristretto et découvrez [The Gimp](#retouchez-vos-images-avec-the-gimp) :)

## Gérer votre photothèque avec Shotwell

**Ouvrir Shotwell : Menu > Graphisme > Shotwell**

**Shotwell** est spécialement conçu pour organiser et classer vos photos de familles 
dans différents dossiers (par défaut dans 'Année/mois/jour') et pour les enregistrer 
dans votre dossier d'Images.

![Shotwell : interface par défaut au premier lancement](img/05-media/15-shotwell-1.png)

Vous pourrez utiliser Shotwell pour importer vos photos depuis un dossier local 
ou votre appareil photo numérique : il suffit de brancher votre APN ou votre 
Smartphone puis de sélectionner votre périphérique depuis le panneau latéral. 
Vous pourrez retoucher vos photos et les exporter sur un compte en ligne.

Shotwell présente une interface sombre par défaut mais vous pouvez configurer 
l'apparence du logiciel depuis le menu "Édition > Préférences".

![Shotwell : préférences et passage en mode clair](img/05-media/16-shotwell-2.png)

## Le croquis simple avec Dessin

**Ouvrir Dessin : Menu > Graphisme > Dessin**

**Dessin** ou **Drawing** est un logiciel minimaliste de dessin qui permet de 
prendre rapidement en main le dessin sur ordinateur, avec la souris ou une tablette 
graphique.  
**Dessin** n'est pas aussi complet que The Gimp (présenté dans le chapitre suivant), 
mais vous pourrez vous amuser à anoter vos photos, réaliser quelques croquis ou modifier 
des images existantes.

Le menu d'aide n'est pas encore traduit en français, mais les options des outils sont 
peu nombreuses et très intuitives.

Le panneau latéral gauche présente les outils disponibles tandis que la barre d'état 
en bas de la fenêtre vous permet de sélectionner vos options (couleurs de premier et 
d'arrière plan, taille du crayon ou du pinceau, police de caractères à utiliser...)

![Le logiciel Dessin](img/05-media/17-dessin.png)

## Retouchez vos images avec The Gimp

**Ouvrir Gimp : Menu > Graphisme > Éditeur d'image Gimp**

**GIMP** (GNU Image Manipulation Program) est un outil d'édition et de retouche 
d'image, diffusé sous la licence GPLv3 comme un logiciel gratuit et libre.

GIMP est utilisé pour la retouche et l'édition d'images, le dessin à main levée,
réajuster, rogner, faire des photomontages, convertir entre différents formats 
d'images, et d'autres tâches spécialisées. Les images animées comme les fichiers 
GIF et MPEG peuvent être créées en utilisant un plugin d'animation.

Les développeurs et mainteneurs de GIMP souhaitent créer un logiciel 
d'infographie gratuit haut de gamme pour l'édition et la création d'images 
originales, de photos, d'icônes, d'éléments graphiques de pages web, et d'art 
pour les éléments de l'interface de l'utilisateur (dixit Wikipédia).

Gimp est-il un logiciel pour les débutants ? Clairement non ! 
Mais que fait-il dans DFiso alors ?  
Gimp est une application phare dans l'univers des logiciels libres et est 
relativement facile à prendre en main, surtout avec les tutoriels proposé par le 
site principal.

N'hésitez pas à parcourir 
[le guide de l'utilisateur](https://docs.gimp.org/2.10/fr/index.html) (en français) 
ou la 
[documentation dédiée sur Debian-Facile](https://debian-facile.org/doc:media:gimp). 
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/mbvQ8n196YtfGkYnToVLqe)**

![The Gimp : interface par défaut](img/05-media/18-gimp.png)

The Gimp propose une interface sombre par défaut mais vou pourrez passer en mode 
clair depuis le menu Édition > Préférences > Thème.

![The Gimp : préférences et passage en mode clair](img/05-media/19-gimp-pref.png)

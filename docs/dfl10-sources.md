# Sources & licence

Les sources utilisées pour construire les images ISOs et la documentation dédiée 
sont disponibles sur l'espace git dédié : 
[https://salsa.debian.org/debian-facile-team/projetsdf](https://salsa.debian.org/debian-facile-team/projetsdf)

**Libre...**

Ce manuel est édité en [markdown](https://fr.wikipedia.org/wiki/Markdown) 
et exporté en PDF grâce à [pandoc](http://pandoc.org/).

Debian-Facile distribue des ISOs et des outils sous licence libre 
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html)

La documentation est distribuée sous licence libre 
[WTFPL](http://www.wtfpl.net/).

Les dessins et avatar de [Péhä](https://lesptitsdessinsdepeha.wordpress.com/) 
(merci@lui) sont publiés sous licence 
[CC-BY-SA.](https://creativecommons.org/licenses/by-sa/3.0/deed.en)

![avatar dflinux par Péhä](img/logos/df-buster-avatar.png)


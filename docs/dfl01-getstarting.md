# Premier lancement

## L'écran de connexion

Votre écran de connexion s'affiche au démarrage de votre système afin de 
vous permettre de vous identifier (identifiant / mot de passe). Si vous 
souhaitez le configurer pour activer la connexion automatique par exemple,
rendez-vous au [chapitre 7.2](#configurez-votre-ecran-de-connexion).

![L'écran de connexion](img/01-start/lightdm.png)

Notez que vous pouvez éteindre ou redémarrer votre ordinateur depuis cet 
écran, en cliquant sur le bouton d'extinction en haut à droite.  
Si vous installez un environnement de bureau supplémentaire comme Gnome ou
LXQt, il sera disponible depuis cet écran via le bouton de session en haut 
à droite.

## Le message d'accueil

Au premier lancement en live ou après l'installation, un message d'accueil 
s'affiche pour vous présenter brièvement votre système Debian, personnalisé 
par Debian-Facile.

![Le message d'accueil](img/01-start/welcome.png)

Comme il est indiqué dans ce message, n'hésitez pas à venir vous inscrire 
sur [le forum debian-facile](https://debian-facile.org/forum.php) afin de 
partager votre expérience, trouver de l'aide... et des humains :).


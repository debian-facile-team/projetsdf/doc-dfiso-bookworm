# Applications Bureautique

## Éditez un texte avec Mousepad

**Ouvrir Mousepad : Menu > Accessoires > Mousepad**

Mousepad est un éditeur de texte minimal mais fonctionnel, parfaitement suffisant 
pour écrire une simple lettre ou une ébauche de tout travail d'édition.  
Il dispose d'une barre de menu simple vous permettant les actions de base 
(copier/coller/supprimer/imprimer) mais aussi la fonction de 
recherche/remplacement ainsi que la numérotation des lignes ou la modification 
de la police d'écriture.

![Mousepad : l'éditeur de texte](img/04-office/01-mousepad.png)

## Suite bureautique complète LibreOffice

**Ouvrir LibreOffice : Menu > Bureautique > LibreOffice**

[LibreOffice](http://fr.libreoffice.org) est une suite bureautique libre et 
gratuite qui offre des modules de traitement de texte (Writer), tableur (Calc), 
présentation (Impress), dessin vectoriel (Draw), et édition de formules 
mathématiques. 

![LibreOffice](img/04-office/02-libreoffice.png)

Les possibilités de LibreOffice sont nombreuses. Le 
[wiki officiel](https://wiki.documentfoundation.org/FR/FAQ) de la communauté 
francophone est très bien fait et vous apportera une aide efficace et complète.

## La prise de notes avec Gnote

**Ouvrir Notes : Menu > Accessoires > Gnote**

La prise de notes rapides est assurée par Gnote. Ce logiciel vous permet de créer, 
éditer et modifier une ou plusieurs notes et de les synchroniser avec vos services 
en ligne (notes Gmail, calendrier WebDav, etc).

La configuration de Gnote passe par son menu à 3 bandes en haut à droite. Il vous 
permettra de choisir le dossier de synchronisation ou d'utiliser Gnote simplement 
en local pour une prise de note rapide et simplifiée. Notez que vous pourrez insérer 
des liens entre les notes ou vers des adresses en ligne (URL).

Vous pourrez également exporter vos notes au format HTML pour une diffusion en 
ligne, modifier la police d'affichage ou classer vos notes par catégories.

![Gnote : la prise de note rapide et synchronisée](img/04-office/03-gnote.png)

## Visionneur de PDF et eBooks Atril

**Ouvrir Atril : Menu > Bureautique > Visionneur de documents Atril**

DFiso intègre Atril, un outil simple pour visionner vos documents PDF. Il se 
lance directement lors d'un double-clic sur un fichier de type PDF ou ePub (livre 
numérique) et vous donne accès aux fonctions de base (simple ou double page, table 
des matières, navigation, marque-pages…)

![Atril : visionner des PDF](img/04-office/04-atril.png)

## Manipuler les PDF avec Xournal et PDFarranger

Vous devrez parfois remplir ou compléter des fichiers PDF, ce qui est normalement possible 
grâce à une visionneuse de PDF standard. Il arrive que le formatage du PDF soit de mauvaise 
qualité, rendant impossible l'édition via les outils classiques.  
DFiso intègre deux petits outils pour vous permettre de résoudre ce genre de soucis :

* **Xournal**, disponible dans la section "accessoires" de votre menu, vous permettra d'annoter 
vos fichiers PDF comme avec un logiciel graphique ;
* **PDFarranger**, disponible depuis la section "office" de votre menu, vous permettra de réarranger 
vos fichiers PDF volumineux.

## Imprimer et Numériser

### Configurer une imprimante

**Ouvrir l'outil de configuration de l'imprimante : 
Menu > Paramètres > configuration de l'impression**

Votre Debian personnalisée par Debian-Facile intègre tous les éléments nécessaires à 
l'impression et à la numérisation de documents.

N'oubliez pas de brancher, allumer et connecter votre imprimante avant de lancer
l'outil de configuration.

La première fenêtre qui s'ouvre vous permet d'ajouter une imprimante avec 
"Ajouter". Si le bouton n'est pas disponible, cliquez sur "Déverrouiller" :

![Impression : ajouter une imprimante](img/04-office/05-print-1.jpg)

Vous accédez à la fenêtre "Nouvelle imprimante" qui vous présente la liste des 
imprimantes détectées en local ou sur le réseau. Sélectionnez votre imprimante 
puis cliquez sur "Suivant" :

![Impression : sélectionner une imprimante](img/04-office/06-print-2.jpg)

Si votre imprimante a été correctement détectée, le pilote a été choisi pour 
vous. Vous pouvez décrire votre imprimante dans la fenêtre suivante. Une fois 
vos modifications effectuées, cliquez sur "Appliquer". L'installeur vous proposera 
alors de tester votre imprimante avec une page d'essai :

![Impression : description de l'imprimante](img/04-office/07-print-3.jpg)

![Impression : page de test](img/04-office/08-print-4.jpg)

Votre imprimante est désormais ajoutée à votre système. Elle sera disponible 
pour vos tâches d'impression depuis tous les programmes installés (Libreoffice, 
Evince pour les pdf, etc).  
Pour configurer votre imprimante, faites un double-clic sur son icône.

Petite astuce : pour changer le comportement en cas d'erreur, cliquez sur votre 
imprimante, vous arrivez sur la page de réglage de votre imprimante. Cliquez 
sur "Comportements" et remplacez "Arrêter l'imprimante" par "Abandonner la tâche".

![Impression : configurer une imprimante](img/04-office/09-print-5.jpg)

### Scanner un document

**Ouvrir Simple Scan : Menu > Graphisme > Outil de numérisation Simple Scan**

La numérisation est confiée à simple scan. Comme son nom l'indique, 
son utilisation est… simple.  
Connectez votre scanner avant de lancer simple scan. 
Mettez votre feuille à scanner dans votre scanner, puis cliquez sur le bouton 
"Numériser". Le scanner chauffe et affiche l'aperçu :

![SimpleScan : affichage de l'aperçu](img/04-office/10-scan1.png)

Optionnel : si vous souhaitez scanner un document à plusieurs pages, vous pouvez 
placer la page suivante dans votre scanner et cliquer à nouveau sur "numériser". 
SimpleScan affichera la nouvelle page à la suite.

![SimpleScan : Scan de plusieurs feuilles](img/04-office/11-scan2.png)

Les boutons d'outils en bas de la fenêtre vous permettent plusieurs opérations :  
- Les deux flèches permettent de pivoter votre page scannée si l'aperçu n'est 
pas correctement orienté.  
- Les ciseaux permettent de recadrer votre page scannée. Cliquez dessus et un 
cadre apparaîtra sur votre aperçu. Il vous suffit de déplacer ou redimensionner 
le cadre pour obtenir le cadrage désiré. (Ce qui est hors du cadre ne sera pas 
conservé)  
- La poubelle permet de supprimer une feuille scannée.

Important : Si vous avez scanné plusieurs feuilles, cliquez sur la feuille que 
vous souhaitez modifier avant de choisir un outil.

![SimpleScan : rotation et recadrage](img/04-office/12-scan3.png)

Une fois le résultat désiré obtenu, cliquez sur le bouton "Enregistrer".
Une fenêtre d'enregistrement s'affiche : vous pouvez nommer votre fichier 
numérisé en haut de la fenêtre. Le nom par défaut est "Document numérisé".

Vous pouvez choisir dans quel dossier enregistrer votre document. Par défaut, 
il le sera dans votre dossier "Documents".

Enfin, vous pouvez choisir le format d'enregistrement dans la liste déroulante 
en bas à gauche de la fenêtre.  
- Si vous scannez un document contenant du texte, préférez le format PDF (document).  
- Si vous scannez une photo, préférez le format JPEG (image).

![SimpleScan : enregistrer le document](img/04-office/13-scan4.png)


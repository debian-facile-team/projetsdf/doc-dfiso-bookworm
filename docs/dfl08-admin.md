# Administration du système

Les tâches d'administration de votre système nécessitent un accès administrateur. 
Votre mot de passe utilisateur vous sera demandé pour ces tâches.

## Mises à jour

Les mises à jour Debian n'ont rien à voir avec celles de Windows…

Les mises à jour sur les systèmes GNU/Linux sont l'expression directe d'une des 
forces du logiciel libre: les sources publiques.

Alors que les systèmes comme Windows cachent le plus longtemps possible les failles
de leurs systèmes (puisqu'ils vendent leurs systèmes), les distributions libres 
annoncent immédiatement les failles détectées et les corrigent dans la foulée ! 
D'où l'importance de faire régulièrement vos mises à jour, afin de garder un 
système le plus sécurisé possible.

**Par défaut, votre système Debian-Facile est configuré pour vérifier régulièrement 
les mises à jour disponibles et les insaller automatiquement.**  
Tout est fait en «tâche de fond» pendant votre utilisation, vous ne subirez donc
pas de ralentissement à l'arrêt ou au redémarrage.

### Paramétrer ou désactiver les mises à jour automatiques

**Gestion des mises à jour : Menu > Paramètres > Software & Updates**

La traduction en Français de cet outil est malheureusement pour le moment 
incomplète. La gestion des mises à jour automatiques s'effectue à l'onglet 
*Updates*.  
Dans cet onglet, deux options permettent de gérer les mises à jour automatiques :

![Préférences des mises à jour](img/08-admin/01-software-properties-updates.png)

**Fréquence des vérifications de mises à jour.**

Le paramètre *Automatically check for updates* (vérifier automatiquement les mises 
à jour) définit la fréquence à laquelle votre système Debian recherche des mises à 
jour. Les options possibles sont :  
- **Choix par défaut :** Quotidiennement,  
- Tous les deux jours,  
- Hebdomadaire,  
- Toutes les deux semaines,  
- *Never* (jamais).

**Action automatique en cas de mise à jour disponible**

Le paramètre *When there are security updates* (lorsqu'il y a des mises à jour de 
sécurité) détermine l'action automatique en cas de mises à jour de sécurité 
disponibles. Les options possibles sont :  
- *Display immediatly* : afficher immédiatement, et vous proposer de les 
télécharger et les installer manuellement.  
- *Download automatically* : télécharger automatiquement. L'installation manuelle 
vous sera ensuite proposée.  
- **Choix par défaut > *Download and install automatically* :** Télécharger et 
installer automatiquement.

### Mise à jour manuelle

**Mise à jour manuelle : Menu > Système > Logiciels**

La section dédiée "Mises à jour" de "Logiciels" vous indiquera la/les mise(s) à jour
disponible(s) et/ou déjà téléchargée(s). Si aucune mise à jour n’est disponible, 
vous pouvez vérifier les dépôts grâce au bouton dédié en haut à gauche.  
![](img/08-admin/logiciels-refresh.jpg)

Dans notre exemple, une mise à jour incluant la “mise à jour du système 
d’exploitation” exige un redémarrage. On commence par télécharger les paquets à 
mettre à jour s'ils ne le sont pas déjà puis il vous reste alors à redémarrer 
en cliquant sur le bouton dédié.

![Logiciels : appliquer la mise à jour et redémarrer](img/08-admin/03-logiciels-upgrade-2.png)

Notez que pour des mises à jour moins importantes, le redémarrage ne sera pas nécessaire.

![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/52e9442c-e7ee-4b28-8564-f77bfe78aa31)** : mises à jour avec un terminal.

## Gestion des applications

### Pour les anciens utilisateurs de Windows

L'installation de logiciels est fondamentalement différente sous Windows et sur 
une distribution Linux, comme ici Debian :

Sous Windows, vous aviez certainement l'habitude d'installer vos logiciels favoris 
après les avoir téléchargés sur des sites Internet. Ce qui pose des problèmes 
majeurs de sécurité :  
- Aucun contrôle de sécurité n'est fait par Windows sur les logiciels que vous 
installez. Une aubaine pour la propagation de virus et de logiciels malveillants.  
- Les logiciels ne sont pas automatiquement mis à jour par Windows, ce qui nuit 
entre autre à la correction des failles de sécurité.

Avec Debian, votre système télécharge uniquement vos logiciels sur des serveurs 
sécurisés (les dépôts). Ainsi, tous les logiciels qui s'y trouvent sont 
majoritairement sous licence libre, et ont été approuvés par Debian. Vous pouvez 
donc les télécharger sans risque.

En contrepartie, vous devrez oublier votre habitude de rechercher vos logiciels 
sur des sites Internet. C'est le prix à payer pour un système sécurisé :)

Les logiciels privateurs que vous utilisiez auparavant sous Windows pourraient ne 
plus être compatible avec Debian. Le mieux est de s'orienter vers une alternative 
libre.

- l'association Framasoft tient un annuaire des alternatives aux logiciels 
privateurs : [https://framalibre.org/alternatives](https://framalibre.org/alternatives).
- Si vous ne trouvez pas d'alternatives à vos logiciels, vous pouvez demander 
de l'aide sur le forum Debian-Facile : 
[https://debian-facile.org/forum.php](https://debian-facile.org/forum.php)

### La logithèque simplifiée Debian

**Logithèque simplifiée Debian : Menu > Système > Logiciels**

Votre système embarque "Logiciels" qui vous propose une interface 
simplifiée pour débuter dans la recherche et l’installation de logiciels.

![Logiciels : le gestionnaire d'applications](img/08-admin/04-gnome-software1.png)

L'interface de Logiciels vous permet de rechercher un logiciel grâce à 
son nom simplement en cliquant sur la loupe en haut à gauche :

![Logiciels : rechercher une application](img/08-admin/05-gnome-software2.png)

Vous pouvez aussi parcourir les catégories de logiciels :

![Logiciels : catégories d'applications](img/08-admin/06-gnome-software3.png)

À la sélection d'une application : si elle est déjà installé, Logiciels vous 
propose de la supprimer; dans le cas contraire, vous pouvez l'installer... 
c'est aussi simple que ça :

![Logiciels : installer ou désinstaller un logiciel](img/08-admin/07-gnome-software4.png)

Comme tous les autres outils d'administration, le mot de passe administrateur vous 
sera demandé et une confirmation sera affichée pour chaque opération.

#### Logiciels : provenance des logiciels (Debian ou Flatpak)

En recherchant un logiciel, vous pourrez voir figurer la mention "Source:dl.flathub.org"

![Logiciels : Plusieurs provenances de logiciels possibles](img/08-admin/08-flatpak1.png)

Cette mention signifie que ce logiciel provient d'une structure indépendante 
de Debian, appelée Flatpak.  
Debian-Facile a paramétré "Logiciels" afin que vous puissiez installer ces
logiciels, car cela vous permettra d'installer des versions plus récentes que 
celles fournies par Debian.

Cela vous donnera aussi accès à des logiciels normalement non-disponible sous 
Debian, comme Anydesk (logiciel d'assistance à distance), Discord et Skype 
(logiciels de communication), Spotify (logiciel d'écoute de musique en ligne), 
Steam (magasin de jeux PC) etc.

![Logiciels : Skype, disponible uniquement via Flatpak](img/08-admin/09-flatpak2.png)

Il y a cependant des inconvénients à connaître :  
- Les logiciels Flatpak ne sont pas vérifiés par l’équipe de sécurité de Debian. 
- Un logicel Flatpak occupe beaucoup plus d'espace disque qu'un logiciel Debian 
(car il s'installe avec toutes ses dépendances).  
- Certains logiciels distribués par Flatpak sont des logiciels propriétaires 
controversés (Skype, connu pour sa collecte de données personnelles par exemple).

Pour ces raisons, Debian-Facile vous recommande de privilégier les applications 
issues de Debian.  
En connaissant ces données, vous serez en mesure de savoir si vous souhaitez 
installer une application issue de Debian ou de Flatpak.

Plus d'informations sur Flatpak dans les cahiers du débutant, chapitre 8.8 :)

## Gestion des utilisateurs

**Gestion des utilsateurs : Menu > Paramètres > Utilisateurs et groupes**

Si vous n'êtes pas le seul utilisateur de votre ordinateur, vous pouvez créer de
nouveaux utilisateurs afin de préserver vos données et vos préférences.  
En ajoutant un utilisateur, vous allez créer un nouveau dossier dans votre 
système. Ce dossier, au nom du nouvel utilisateur, reprendra les paramètres par 
défaut livrés lors de l'installation.  
Il sera accessible en lecture (vous pouvez consulter les données de l'autre 
utilisateur), mais pas en écriture (vous ne pouvez pas créer ou modifier ses 
données).

Votre système Debian dispose d'un outil graphique simple pour effectuer cette tâche.
La fenêtre qui s'affiche alors présente votre compte utilisateur et ses 
spécificités. Vous pouvez donc modifier les paramètres de votre compte ou en 
créer un autre. Votre mot de passe vous sera demandé.  
![](img/logos/df-tutos.png) **> [tutoriel vidéo](https://video.tedomum.net/w/bf8e3410-b79d-48ef-95c5-6378232b2abf)**

- Pour ajouter un utilisateur, clic sur "Ajouter" dans la colonne de gauche :

![ajouter un utilisateur](img/08-admin/10-users1.png)

- Vous devrez renseigner le nom complet du nouvel utilisateur ainsi que son 
identifiant (le login utilisé à la connexion de la session) :

![création du nouvel utilisateur](img/08-admin/11-users2.png)

- Vient ensuite le choix du mot de passe (à remplir à la main, mais vous pouvez 
aussi le générer automatiquement) :

![mot de passe du nouvel utilisateur](img/08-admin/12-users3.png)

- L'utilisateur sera créé... et voilà :

![nouvel utilisateur créé](img/08-admin/13-users4.png)

Les paramètres avancés vous permettent de définir plus précisément les droits et
possibilités de chaque utilisateur.

## Gestion des mots de passe

DFiso intègre **KeePassXC**, un gestionnaire de mots de passe qui vous permettra 
de conserver tous vos mots de passe en sécurité derrière un "mot de passe maître". 
KeePassXC peut aussi être lié à votre navigateur internet afin de sécuriser vos 
mots de passes numériques.

La prise en main de KeePassXC est simple. L'association 
[Céméa](https://cemea.asso.fr/) a réalisé un excellent tutoriel en PDF que vous 
pouvez trouver directement dans votre dossier personnel. Bonne lecture.

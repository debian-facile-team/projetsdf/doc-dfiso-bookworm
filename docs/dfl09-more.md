# Aller plus loin avec Debian

## Anonymat et vie privée

L'actualité est claire : Internet est sur écoute. Ce n'est pas pour vous alarmer
ou vous faire peur, c'est simplement pour bien prendre conscience que 
"l'internet" n'est pas votre salon privé et que chaque photo ou texte porté sur 
la toile est potentiellement récupérable et exploitable.

Afin de préserver votre vie privée (et celle de vos contacts) ou sécuriser votre
navigation sur le réseau, nous vous conseillons de consulter le chapitre 10 des 
cahiers du débutant.

## Le Terminal

Ce système personnalisé par Debian-Facile est un projet qui met l'accent sur les 
solutions graphiques *avélasouris*
et tente au maximum de vous éviter l'utilisation du terminal si vous n'en avez 
pas envie. Cependant, si l'envie vous prend, sachez que le terminal reste 
l'outil le plus rapide et le plus efficace pour l'administration et la 
maintenance de votre système, ainsi que pour effectuer des tâches "en lot" sur 
un grand nombre de données.

Les interfaces graphiques ne peuvent pas afficher l'intégralité des options 
possibles de chaque programme, le terminal, oui. Grâce au terminal, vous prenez 
le plein contrôle de votre système.

Pour commencer à découvrir cet outil indispensable si vous souhaitez découvrir 
un peu mieux le système Debian GNU/linux, et ainsi acquérir votre autonomie 
numérique, nous vous laissons parcourir le chapitre 3.8 des cahiers du débutant.

## Supprimer les outils de Debian-Facile

Debian-Facile livre des images ISO Debian mais effectivement, pas une Debian 
"pure"...  
Si vous souhaitez vous détacher des outils facilitants et "passer" sur une Debian 
pure, voici quelques commandes qui supprimeront les paquets dédiés.

Depuis un terminal administrateur, pour supprimer les paquets, lancez :

    apt autoremove --purge lescahiersdudebutant df-manuel

Pour finir le nettoyage, toujours depuis votre terminal administrateur, 
lancez cette commande :

    rm -Rf /usr/share/dfiso

Il ne restera que quelques icônes et les fonds d'écran intégrés... :)


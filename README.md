## Documentation utilisateur du projet DFiso 

Documentation exportée en PDF et intégrée aux ISOs du [projet DFiso](http://debian-facile.org/projets:iso-debian-facile)

Dépendances pour construire la doc :

    pandoc texlive-xetex texlive-lang-french fonts-hack-otf fonts-hack-ttf fonts-hack-web

Dépendance pour construire le paquet Debian :

    equivs

----
